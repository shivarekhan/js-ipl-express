const express = require('express');
const path = require("path");

const app = express();
const port = process.env.PORT || 8000;

const pathForEntryHtml = path.join(__dirname, "/src/public/index.html");

app.get('/', function (req, res) {
    res.sendFile(pathForEntryHtml);
})


app.get('/:name', function (req, res) {
    const fileName = req.params.name;
    const filePath = path.join(__dirname, "src/public/output/", fileName+".json");
    console.log(filePath);
    res.sendFile(filePath);
});



app.listen(port, function () {
    console.log(`server running on the port number ${port}`);
})