function timesEachTeamWonTossAndMatch(matches) {
  if (matches === undefined || !Array.isArray(matches)) {
    return {};
  }
  let result = matches.reduce((accumelator, currentObject) => {
    if (currentObject.winner === currentObject.toss_winner) {
      accumelator[currentObject.toss_winner] =
        (accumelator[currentObject.toss_winner] || 0) + 1;
    }
    return accumelator;
  }, {});
  return result;
}

module.exports = timesEachTeamWonTossAndMatch;
