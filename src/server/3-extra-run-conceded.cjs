function extraRunConcede(deleveries, matches, year = "2016") {
  if (!Array.isArray(deleveries) || deleveries === null) {
    return;
  }
  let matchIds = matches
    .filter((match) => match.season == year)
    .map((match) => parseInt(match.id));
  let extraRun = deleveries.reduce((extraRun, currOver) => {
    let matchId = Number(currOver.match_id);

    if (matchIds.includes(matchId)) {
      if (extraRun.hasOwnProperty(currOver.bowling_team)) {
        extraRun[currOver.bowling_team] += Number(currOver.extra_runs);
      } else {
        extraRun[currOver.bowling_team] = Number(currOver.extra_runs);
      }
    }
    return extraRun;
  }, {});
  return extraRun;
}

module.exports = extraRunConcede;
