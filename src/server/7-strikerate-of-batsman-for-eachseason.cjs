function strikeRateOfBatsmanSeason(deleveries, matches) {
  if (
    deleveries === undefined ||
    matches === undefined ||
    !Array.isArray(deleveries) ||
    !Array.isArray(matches)
  ) {
    return {};
  }
  let matchIdWithYear = matches.reduce((matchIdWithYear, currentMatch) => {
    matchIdWithYear[currentMatch.id] = currentMatch.season;
    return matchIdWithYear;
  }, {});

  let totalRunTotalBallPerYear = deleveries.reduce(
    (totalRunTotalBallPerYear, delivery) => {
      const year = matchIdWithYear[delivery.match_id];
      if (!totalRunTotalBallPerYear[delivery.batsman]) {
        totalRunTotalBallPerYear[delivery.batsman] = {};
      }
      if (!totalRunTotalBallPerYear[delivery.batsman][year]) {
        totalRunTotalBallPerYear[delivery.batsman][year] = {
          total_runs: 0,
          total_balls: 0,
        };
      }
      totalRunTotalBallPerYear[delivery.batsman][year]["total_runs"] += isNaN(
        delivery.total_runs
      )
        ? 0
        : Number(delivery.total_runs);
      totalRunTotalBallPerYear[delivery.batsman][year]["total_balls"]++;
      return totalRunTotalBallPerYear;
    },
    {}
  );

  let strikeRateOfBatsman = Object.keys(totalRunTotalBallPerYear).reduce(
    (totalRunTotalBallPerYear, strike) => {
      Object.keys(totalRunTotalBallPerYear[strike]).reduce(
        (totalRunTotalBallPerYear, year) => {
          let totalRuns = totalRunTotalBallPerYear[strike][year]["total_runs"];
          let totalBalls =
            totalRunTotalBallPerYear[strike][year]["total_balls"];

          totalRunTotalBallPerYear[strike][year] = Number(
            (totalRuns * 100) / totalBalls
          );
          return totalRunTotalBallPerYear;
        },
        totalRunTotalBallPerYear
      );
      return totalRunTotalBallPerYear;
    },
    totalRunTotalBallPerYear
  );

  return strikeRateOfBatsman;
}
module.exports = strikeRateOfBatsmanSeason;
