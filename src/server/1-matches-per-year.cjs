function matchPerYearFinder(matches) {
  if (!Array.isArray(matches) || matches === undefined) {
    return;
  }

  let matchesPerYear = matches.reduce((matchesPerYear, currentMatch) => {
    matchesPerYear[currentMatch.season] =
      (matchesPerYear[currentMatch.season] || 0) + 1;
    return matchesPerYear;
  }, {});
  return matchesPerYear;
}

module.exports = matchPerYearFinder;
