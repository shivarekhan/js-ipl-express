function highestPlayerDismisses(deleveries) {
    let playerDismissedAndBowler = deleveries.reduce((playerDismissedAndBowler, delivery) => {
        if (delivery.player_dismissed !== '') {
            if (playerDismissedAndBowler[delivery.player_dismissed] === undefined) {
                playerDismissedAndBowler[delivery.player_dismissed] = {};
            }
            if (playerDismissedAndBowler[delivery.player_dismissed][delivery.bowler] === undefined) {
                playerDismissedAndBowler[delivery.player_dismissed][delivery.bowler] = 0;
            }
            playerDismissedAndBowler[delivery.player_dismissed][delivery.bowler]++;

        }
        return playerDismissedAndBowler;

    }, {})

    let wicketCount = Object.keys(playerDismissedAndBowler)
        .map((outBatsman) => {
            return Object.keys(playerDismissedAndBowler[outBatsman])
                .map((bowler) => {
                    return playerDismissedAndBowler[outBatsman][bowler];
                });

        });

    let highestOutCount = Math.max(...wicketCount.flat());
    let highestOutPlayer = Object.keys(playerDismissedAndBowler)
        .reduce((highestOutPlayer, currentBatsman) => {
            let bowlerWhoDismissed = Object.keys(playerDismissedAndBowler[currentBatsman])
                .filter((currentBowler) => {
                    return playerDismissedAndBowler[currentBatsman][currentBowler] === highestOutCount;
                })

            if (bowlerWhoDismissed.length > 0) {
                highestOutPlayer.push({
                    [currentBatsman]: {
                        'dismissed by': bowlerWhoDismissed,
                        'number of time dismissed': highestOutCount
                    }
                });
            };
            return highestOutPlayer;
        }, []);
    return highestOutPlayer;


}

module.exports = highestPlayerDismisses;